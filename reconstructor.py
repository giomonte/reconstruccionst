import neurolab as nl
import Helper
import numpy as np
np.set_printoptions(threshold=np.inf)
# from pandas import DataFrame

def reconstruct(originalSerie, trainingSetPercent, NI, netFileDir):

    artificialSeries = []
    originalSeries = []

    reconstructedSeries = []
    for i in range( len(originalSerie)):

        original = originalSerie[i]

        if original == "NA" and len(reconstructedSeries) >= NI:

            window = reconstructedSeries[i - NI:i]

            if "NA" not in window:

                input = np.array(window)
                inp = input.reshape(1, NI)
                net = nl.load(netFileDir)
                out = net.sim(inp)

                reconstructedSeries.append(out[0][0])
            else :

                reconstructedSeries.append(original)

        else:
            reconstructedSeries.append(original)


    # print "originalSerie : ", np.reshape(originalSerie, (len(originalSerie), 1))
    # print "reconstructed : ", np.reshape(reconstructedSeries, (len(reconstructedSeries), 1))

    originalSerie = np.reshape(originalSerie, (len(originalSerie), 1))
    reconstructed = np.reshape(reconstructedSeries, (len(reconstructedSeries), 1))

    return originalSerie, reconstructed

if __name__ == "__main__":

    NI = 22
    NH = 18

    serieTiempo = 'wind_cointzio'

    rutaArchivo = 'seriesTiempoConPocosHuecos/'
    destino = 'resultadosSeriesTiempoConPocosHuecos/'+serieTiempo+'/'
    nombreArchivo = "serieTiempoConPocosHuecos_"+serieTiempo+"_10m_muestra"

    print "Reconstruccion de la serie : ", rutaArchivo

    numeroMuestra = 3000
    archivo = Helper.obtenerArchivo( rutaArchivo + nombreArchivo + ".csv", numeroMuestra)

    netFileDir = "redesConPocosHuecos/"+serieTiempo+"/MejorN_" + str(nombreArchivo) + ".net"

    originalSerie, reconstructed = reconstruct(archivo, 0.80, NI, netFileDir)

    nombreArchivoRecontruido = destino + nombreArchivo + "_reconstruidoANN.csv"
    print "Archivo con serie reconstruida con ANN: ", nombreArchivoRecontruido
    Helper.guardarExcel(nombreArchivoRecontruido, originalSerie, reconstructed, 'Original', 'Reconstructed')
