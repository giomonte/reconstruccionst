# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

# Sin reconstruccion
directorio = 'seriesTiempoConPocosHuecos/'

# Con reconstruccion
#directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_aristeomercado/'
#directorio = 'datosExperimentosMuestras/'

# Con reconstruccion
#directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_aristeomercado/'
directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_cointzio/'

numeroMuestra =  3000

# [nombreArchivo, nE, nO, aE]
datosArchivos = np.array([
                        ['wind_cointzio_10m_muestra', 23, 5, 7]
                    ])

for i in range (0, len(datosArchivos)):

    nb_archivo = datosArchivos[i][0]
    rutaArchivo = directorio + nb_archivo+'.csv'

    print "Generando la mejor Red para la serie : "+ rutaArchivo

    # Sin reconstruccion
    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    # Reconstruccion
    #archivo = Helper.funcionLeerArchivo2( rutaArchivo )

    numeroValidacion = int(( len(archivo) * 0.20 ))

    numeroTraining = numeroMuestra - numeroValidacion

    arrayError  = []
    arrayNet    = [] 

    for j in range(0,3):

        error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), 6000, archivo, numeroTraining)

        print "Error ", (j+1)," : ", error

        arrayError.append(error)
        arrayNet.append(net)

    mejorError = min(arrayError)

    print "Mejor Error: ", mejorError

    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    mejorNet.save(('resultadosSeriesTiempoConPocosHuecos/wind_corrales/MejorN_'+ str(nb_archivo) +'.net'))
